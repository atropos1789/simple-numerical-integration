#include <stdio.h>
#include "../src/libeuler_functions.h"

double test_function(double t, double y) 
{ 
    /* ANALYTIC SOLUTION: 
    y' = 1+t-2ty
    y = exp(-t^2)int(exp(t^2)(1+t))dt
    */

    double func_value = 1.0 + t - 2.0 * t * y;
    return func_value;
};

int main(void)
{
    int steps = 100000;
    forward_euler(test_function, 0.0, 0.0, 0.001, steps);
    improved_euler(test_function, 0.0, 0.0, 0.001, steps);
    runge_kutta(test_function, 0.0, 0.0, 0.001, steps);
    /* print out known correct values */
    /* figure out how to export the data arrays from the functions and run error analysis on them vs the known correct values */
    return 0;
};
