#!/bin/bash

# Write C code to files
##############################################################################
cat << \EOF > src/test.1.0.0.c
#include "libeuler_functions.h"

int main(void)
{
forward_euler(function, 0, 0, 0.001, 500000);
return 0;
};
EOF

cat << \EOF > src/test.2.0.0.c
#include "libeuler_functions.h"

int main(void)
{
improved_euler(function, 0, 0, 0.001, 500000);
return 0;
};
EOF

cat << \EOF > src/test.3.0.0.c
#include "libeuler_functions.h"

int main(void)
{
runge_kutta(function, 0, 0, 0.001, 500000);
return 0;
};
EOF

##############################################################################

# Use the Makefile to compile the tests (need to make it specify for all tests
make test1
make test2
make test3

# UNTESTED, WILL MERGE IN LATER UPDATES EVENTUALLY
/bin/time -v ./bin/test.1.0.0 > /tmp/test.1.0.0.txt 2> data/time.1.0.0.txt
/bin/time -v ./bin/test.2.0.0 > /tmp/test.2.0.0.txt 2> data/time.2.0.0.txt
/bin/time -v ./bin/test.3.0.0 > /tmp/test.3.0.0.txt 2> data/time.3.0.0.txt


mv /tmp/test.1.0.0.txt data/test.1.0.0.txt #copy from ram to disk

mv /tmp/test.2.0.0.txt data/test.2.0.0.txt

mv /tmp/test.3.0.0.txt data/test.3.0.0.txt
