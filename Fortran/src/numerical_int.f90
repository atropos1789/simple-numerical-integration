! Fortran 2003 Free-form


MODULE numerical_int
    use, intrinsic :: iso_c_binding, only: sp=>c_float, dp=>c_double
    implicit none 

CONTAINS

    SUBROUTINE forward_euler
    ! The Euler Method for approximating the solution to the IVP 
    ! y' = f(t,y), y(t_0) = y_0
    ! is
    ! y_{n+1} = y_n + h*f( t_n, y_n )


    END SUBROUTINE forward_euler

    SUBROUTINE improved_euler
    ! The Improved Euler Method for approximating the solution to the IVP 
    ! y' = f(t,y), y(t_0) = y_0
    ! is
    ! k1 = f( t_n, y_n )
    ! k2 = f( t_n + h, y_n + h*k1 )
    ! y_{n+1} = y_n + h*1/2*( k1 + k2 )
    

    END SUBROUTINE improved_euler

    SUBROUTINE runge_kutta
    ! The Fourth Order Runge-Kutta approximation to the IVP 
    ! y' = f(t,y), y(t_0) = y_0
    ! is
    ! k1 = f( t_n, y_n ) 
    ! k2 = f( t_n + 0.5*h, y_n + 0.5*h*k1 )
    ! k3 = f( t_n + 0.5*h, y_n + 0.5*h*k2 )
    ! k4 = f( t_n + h, y_n + h*k3 )
    ! y_{n+1} = y_n + h*1/6*( k1 + 2*k2 + 2*k3 + k4 )
    

    END SUBROUTINE runge_kutta

END MODULE numerical_int
